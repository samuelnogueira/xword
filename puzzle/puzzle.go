package puzzle

import (
	"log"
)

const blackSquare = '■'
const emptySquare = '_'

type Puzzle struct {
	boxes []rune
	lines []line
	slots []slot
	cols  int
	rows  int
}

func NewPuzzle(cols int, rows int) *Puzzle {
	p := Puzzle{
		boxes: make([]rune, cols * rows),
		cols: cols,
		rows: rows,
	}

	for i := range p.boxes {
		p.boxes[i] = emptySquare
	}

	return &p
}

func (puzzle *Puzzle) FillSquares() {
	puzzle.calculateLines()
	for _, line := range puzzle.lines {
		line.putRandomSquare(puzzle)
	}
	puzzle.calculateSlots()
}

func (puzzle Puzzle) Fill(dic Dictionary) bool {
	log.Println("\n" + puzzle.String())
	s := puzzle.findNonFilledSlot()
	if s == nil {
		return true
	}
	words := dic.GetWordsByExpr(s.getExpr(puzzle.boxes))
	for _, word := range *words {
		s.put(puzzle.boxes, word)

		if puzzle.isValid(dic) {
			copy(puzzle.boxes, puzzle.boxes)
			if puzzle.Fill(dic) {
				return true
			}
		}
	}

	return false
}

func (puzzle *Puzzle) String() string {
	s := ""
	for i, r := range puzzle.boxes {
		if i > 0 && i % puzzle.cols == 0 {
			s += "\n"
		}
		s += string(r) + " "
	}

	return s
}

//func (puzzle *Puzzle) Clues(dic Dictionary) string {
//	s := "Horizontal:\n"
//	n := 1
//	for i, slot := range puzzle.slots {
//		if n > 1 && slot.isVertical() {
//			s += "\nVertical:\n"
//			n = 1
//		}
//		s +=
//	}
//
//	return s
//}


func (puzzle *Puzzle) isValid(dic Dictionary) bool {
	for _, s := range puzzle.slots {
		if !dic.HasExpr(s.getExpr(puzzle.boxes)) {
			return false
		}
	}

	return true
}

func (puzzle *Puzzle) calculateSlots() {
	puzzle.slots = make([]slot, 0)
	for _, line := range puzzle.lines {
		puzzle.slots = append(puzzle.slots, line.slots(puzzle.boxes)...)
	}
}

func (puzzle *Puzzle) findNonFilledSlot() *slot {
	for i := range puzzle.slots {
		if !puzzle.slots[i].isFilled(puzzle.boxes) {
			return &puzzle.slots[i]
		}
	}

	return nil
}

func (puzzle *Puzzle) calculateLines() {
	puzzle.lines = make([]line, puzzle.rows + puzzle.cols)

	// add rows
	for y := 0; y < puzzle.rows; y++ {
		puzzle.lines[y] = make([]int, puzzle.cols)
		for x := 0; x < puzzle.cols; x++ {
			puzzle.lines[y][x] = (y * puzzle.rows) + x
		}
	}

	// add cols
	for x := 0; x < puzzle.cols; x++ {
		i := x + puzzle.rows
		puzzle.lines[i] = make([]int, puzzle.rows)
		for y := 0; y < puzzle.rows; y++ {
			puzzle.lines[i][y] = (y * puzzle.rows) + x
		}
	}
}
