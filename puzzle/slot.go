package puzzle

type slot []int

func (s slot) put(boxes []rune, word string) {
	for i := range word {
		boxes[s[i]] = rune(word[i])
	}
}

func (s slot) isFilled(boxes []rune) bool {
	for _, i := range s {
		if boxes[i] == emptySquare {
			return false
		}
	}

	return true
}

func (s slot) isVertical() bool {
	return s[1] - s[0] > 1
}

func (s slot) getExpr(boxes []rune) string {
	expr := ""
	for _, i := range s {
		expr += string(boxes[i])
	}

	return expr
}
