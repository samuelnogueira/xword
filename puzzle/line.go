package puzzle

import "math/rand"

type line []int

func (l line) putRandomSquare(p *Puzzle) {
	i := l[rand.Intn(len(l))]
	p.boxes[i] = blackSquare
}

func (l line) slots(boxes []rune) []slot {
	slots := make([]slot, 0)
	appendSlot := func(as slot) {
		if len(as) > 1 {
			slots = append(slots, as)
		}
	}
	s := slot{}

	for _, i := range l {
		switch boxes[i] {
		case blackSquare:
			appendSlot(s)
			s = slot{}
		default:
			s = append(s, i)
		}
	}

	appendSlot(s)

	return slots
}
