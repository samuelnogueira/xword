package puzzle

import (
	"database/sql"
	"fmt"
)

type Dictionary interface {
	GetWordsByExpr(expr string) *[]string
	GetClueForWord(word string) string
	HasExpr(word string) bool
}

type mysqlDictionary struct {
	Db                 *sql.DB
	TableName          string
	WordColumnName     string
	ClueColumnName     string
	MaxResults         int
	wordCache          map[string]bool
	stmtHasWord        *sql.Stmt
	stmtGetWordsByExpr *sql.Stmt
}

func NewMysqlDictionary(db *sql.DB) *mysqlDictionary {
	md := mysqlDictionary{
		Db: db,
		TableName: "clues",
		WordColumnName: "word",
		ClueColumnName: "clue",
		MaxResults: 500000,
	}
	md.wordCache = map[string]bool{}
	md.stmtHasWord, _ = db.Prepare(
		fmt.Sprintf(
			"SELECT EXISTS(SELECT 1 FROM `%s` WHERE `%s` LIKE ?)",
			md.TableName,
			md.WordColumnName,
		),
	)
	md.stmtGetWordsByExpr, _ = db.Prepare(
		fmt.Sprintf(
			"SELECT `%s` FROM `%s` WHERE `%s` LIKE ? ORDER BY RAND() LIMIT %d",
			md.WordColumnName,
			md.TableName,
			md.WordColumnName,
			md.MaxResults,
		),
	)

	return &md
}

func (md *mysqlDictionary) GetWordsByExpr(expr string) *[]string {
	rows, err := md.stmtGetWordsByExpr.Query(expr)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	words := make([]string, 0, 10)
	for rows.Next() {
		var word string
		if err := rows.Scan(&word); err != nil {
			panic(err)
		}
		words = append(words, word)
	}
	if err := rows.Err(); err != nil {
		panic(err)
	}

	return &words
}

func (md *mysqlDictionary) GetClueForWord(word string) string {
	var clue string
	md.Db.
		QueryRow("SELECT " + md.ClueColumnName + " FROM " + md.TableName + " WHERE " + md.WordColumnName + " = ? LIMIT 1", word).
		Scan(&clue)

	return clue
}

func (md *mysqlDictionary) HasExpr(expr string) bool {
	if _, ok := md.wordCache[expr]; !ok {
		var exists bool
		md.stmtHasWord.QueryRow(expr).Scan(&exists)

		md.wordCache[expr] = exists
	}

	return md.wordCache[expr]
}