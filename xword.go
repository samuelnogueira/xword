package main

import (
	"os"
	"flag"
	"bitbucket.org/samuelnogueira/xword/puzzle"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
	"log"
	"math/rand"
	"time"
)

var (
	DbDsn = os.Getenv("MYSQL_DSN")
)

func main() {
	// parse arguments from command line
	cols := flag.Int("cols", 10, "Columns")
	rows := flag.Int("rows", 10, "Rows")
	flag.Parse()

	// open database connection
	db, err := sql.Open("mysql", DbDsn)
	if err != nil {
		log.Fatalln("Unable to open connection to DB: " + err.Error())
	}
	defer db.Close()

	// create dictionary
	dic := puzzle.NewMysqlDictionary(db)

	rand.Seed(time.Now().UnixNano())

	p := puzzle.NewPuzzle(*cols, *rows)
	p.FillSquares()
	p.Fill(dic)

	fmt.Printf("%+v", p)
}
